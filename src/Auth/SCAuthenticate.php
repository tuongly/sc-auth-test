<?php


namespace Epsilo\Auth;

use Exception;

class SCAuthenticate
{
    const CHANNEL_SENDO = "sendo";
    const CHANNEL_SHOPEE = "shopee";

    static function getInstanceByChannel($channel): IAuthen
    {
        switch ($channel) {
            case self::CHANNEL_SENDO:
                return new SendoAuth();
            case self::CHANNEL_SHOPEE:
                return new ShopeeAuth();
        }
        throw new Exception("channel not found");
    }
}