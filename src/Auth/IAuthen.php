<?php


namespace Epsilo\Auth;


interface IAuthen
{
    function auth($userName, $password, $country = '', $otp = '', $options = []);
}