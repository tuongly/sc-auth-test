<?php

namespace Epsilo\Auth;

trait AuthResponse
{

    protected function success($cookieString, $accountType, $shopSid, $shopName, $shopList = [], $rawData = [], $message = '')
    {
        return json_encode([
            'success' => true,
            'cookie_string' => $cookieString,
            'account_type' => $accountType,
            'shop_sid' => $shopSid,
            'shop_name' => $shopName,
            'shop_list' => $shopList,
            'raw_data' => $rawData,
            'message' => $message
        ]);
    }

    protected function error($message = '', $rawData = [])
    {
        return json_encode([
            'success' => false,
            'raw_data' => $rawData,
            'message' => $message
        ]);
    }

}