<?php
/**
 * Created by Kieu Trung.
 * User: trung.kieu@epsilo.io
 * Date: 17/11/2020
 * Time: 14:34
 */

namespace Epsilo\Auth\OpenApi;
 
class Lazada
{
    const OAUTH_URL = 'https://auth.lazada.com/oauth/authorize';
    const OAUTH_API = 'https://auth.lazada.com/rest';

    const BASE_URL_INDONESIA = 'https://api.lazada.co.id/rest';
    const BASE_URL_MALAYSIA = 'https://api.lazada.com.my/rest';
    const BASE_URL_PHILIPPINES = 'https://api.lazada.com.ph/rest';
    const BASE_URL_SINGAPORE = 'https://api.lazada.sg/rest';
    const BASE_URL_VIETNAM = 'https://api.lazada.vn/rest';
    const BASE_URL_THAILAND = 'https://api.lazada.co.th/rest';

    /**
     * Indonesia    https://api.lazada.co.id/rest
     * Malaysia     https://api.lazada.com.my/rest
     * Philippines  https://api.lazada.com.ph/rest
     * Singapore    https://api.lazada.sg/rest
     * Vietnam      https://api.lazada.vn/rest
     * Thailand     https://api.lazada.co.th/rest
     */

    /**
     * @var int
     */
    const REQUEST_TIME_OUT = 30;

    private $timeout;
    private $baseUrl;
    private $appKey;
    private $secretKey;
    private $signMethod = 'sha256';

    /**
     * Lazada constructor.
     * @param $appKey
     * @param $secretKey
     * @param null $baseUrl
     * @param int $timeout
     */
    public function __construct($appKey, $secretKey, $baseUrl = null, $timeout = self::REQUEST_TIME_OUT)
    {
        $this->baseUrl = $baseUrl;
        $this->appKey = $appKey;
        $this->secretKey = $secretKey;
        $this->timeout = $timeout;
    }

    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAppKey()
    {
        return $this->appKey;
    }

    /**
     * @param Request $request
     * @param null $accessToken
     * @return bool|mixed|string
     */
    public function execute($request, $accessToken = null)
    {
        $sysParams["app_key"] = $this->appKey;
        $sysParams["sign_method"] = $this->signMethod;
        $sysParams["timestamp"] = $this->microsecondTime();
        if ($accessToken != null) {
            $sysParams["access_token"] = $accessToken;
        }
        $apiParams = $request->udfParams;
        $requestUrl = $request->getBaseUrl();
        $requestUrl = $requestUrl ? $requestUrl : $this->baseUrl;

        if ($this->endWith($requestUrl, "/")) {
            $requestUrl = substr($requestUrl, 0, -1);
        }
        $requestUrl .= $request->apiName;
        $requestUrl .= '?';

        $sysParams["sign"] = $this->generateSign($request->apiName, array_merge($apiParams, $sysParams));
        foreach ($sysParams as $sysParamKey => $sysParamValue) {
            $requestUrl .= "$sysParamKey=" . urlencode($sysParamValue) . "&";
        }
        $requestUrl = substr($requestUrl, 0, -1);
        if ($request->httpMethod == 'POST') {
            $resp = $this->curlPost($requestUrl, $apiParams, $request->fileParams, $request->headerParams);
        } else {
            $resp = $this->curlGet($requestUrl, $apiParams, $request->headerParams);
        }
        return $resp;
    }

    /**
     * @return int
     */
    protected function microsecondTime()
    {
        list($mSec, $sec) = explode(' ', microtime());
        return intval(($mSec + $sec) * 1000);
    }

    /**
     * @param $haystack
     * @param $needle
     * @return bool
     */
    protected function endWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return false;
        }
        return (substr($haystack, -$length) === $needle);
    }

    /**
     * @param $apiName
     * @param $params
     * @return string
     */
    protected function generateSign($apiName, $params)
    {
        ksort($params);
        $stringToBeSigned = '';
        $stringToBeSigned .= $apiName;
        foreach ($params as $k => $v) {
            $stringToBeSigned .= "$k$v";
        }
        unset($k, $v);
        return strtoupper(hash_hmac($this->signMethod, $stringToBeSigned, $this->secretKey));
    }

    /**
     * @param $url
     * @param null $postFields
     * @param null $fileFields
     * @param array $headerFields
     * @return array|bool
     */
    public function curlPost($url, $postFields = null, $fileFields = null, $headerFields = [])
    {
        $delimiter = '-------------' . uniqid();
        $data = '';
        if ($postFields) {
            foreach ($postFields as $name => $content) {
                $data .= "--" . $delimiter . "\r\n";
                $data .= 'Content-Disposition: form-data; name="' . $name . '"';
                $data .= "\r\n\r\n" . $content . "\r\n";
            }
            unset($name, $content);
        }
        if ($fileFields) {
            foreach ($fileFields as $name => $file) {
                $data .= "--" . $delimiter . "\r\n";
                $data .= 'Content-Disposition: form-data; name="' . $name . '"; filename="' . $file['name'] . "\" \r\n";
                $data .= 'Content-Type: ' . $file['type'] . "\r\n\r\n";
                $data .= $file['content'] . "\r\n";
            }
            unset($name, $file);
        }
        $data .= "--" . $delimiter . "--";

        $curl = new \Epsilo\Library\CurlBuilder();
        $curl->setUrl($url)->setTimeout($this->timeout);
        foreach ($headerFields as $key => $value) {
            $curl->header("$key: $value");
        }
        $curl->setMethod('POST')
            ->header('Content-Type: multipart/form-data; boundary=' . $delimiter)
            ->header('Content-Length: ' . strlen($data))
            ->setIsRawPostData()
            ->setParam($data)
        ;
        return $curl->execute();
    }

    /**
     * @param string $url
     * @param array $apiFields
     * @param array $headerFields
     * @return array|bool
     */
    public function curlGet($url, $apiFields = null, $headerFields = [])
    {
        foreach ($apiFields as $key => $value) {
            $url .= "&" . "$key=" . urlencode($value);
        }

        $curl = new CurlBuilder();
        $curl->setUrl($url)->setTimeout($this->timeout);
        if ($headerFields) {
            foreach ($headerFields as $key => $value) {
                $curl->header("$key: $value");
            }
        }
        return $curl->execute();
    }

    public function getAccessToken($code)
    {
        $request = new Request('/auth/token/create');
        $request->addApiParam("code", urlencode($code));
        $request->setBaseUrl(self::OAUTH_API);
        $response = $this->execute($request);
        return $response;
    }

    public function getOauthUrl($handleUrl, $state)
    {
        $URI = self::OAUTH_URL . '?';
        $URI .= 'response_type=code&';
        $URI .= 'force_auth=true&';
        $URI .= 'redirect_uri=' . urlencode($handleUrl) . '&';
        $URI .= 'client_id=' . $this->appKey . '&';
        $URI .= 'state=' . urlencode(json_encode($state)). '&';
        return $URI;
    }

}
