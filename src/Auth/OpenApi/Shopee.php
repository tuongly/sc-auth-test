<?php
/**
 * Created by Kieu Trung.
 * User: trung.kieu@epsilo.io
 * Date: 17/11/2020
 * Time: 15:16
 */

namespace Epsilo\Auth\OpenApi;

class Shopee
{
    const OAUTH_URL = 'https://partner.shopeemobile.com/api/v1/shop/auth_partner';
    const OAUTH_API = 'https://partner.shopeemobile.com/api/v1';

    const BASE_URL_INDONESIA = '';
    const BASE_URL_MALAYSIA = '';
    const BASE_URL_PHILIPPINES = '';
    const BASE_URL_SINGAPORE = '';
    const BASE_URL_VIETNAM = '';
    const BASE_URL_THAILAND = '';

    private $baseUrl;
    private $appKey;
    private $partnerId;

    /**
     * @return mixed
     */
    public function getAppKey()
    {
        return $this->appKey;
    }

    public function getPartnerId()
    {
        return $this->partnerId;
    }

    /**
     * Shopee constructor.
     * @param $appKey
     * @param $partnerId
     * @param null $baseUrl
     */
    public function __construct($appKey, $partnerId, $baseUrl = null)
    {
        $this->baseUrl = $baseUrl;
        $this->appKey = $appKey;
        $this->partnerId = $partnerId;
    }

    public function getOauthUrl($handleUrl, $state)
    {
        $handleUrl = $handleUrl . '?state=' . json_encode($state);
        $URI = self::OAUTH_URL . '?';
        $URI .= 'id=' . $this->partnerId . '&';
        $URI .= 'token=' . hash('sha256', $this->appKey . $handleUrl) . '&';
        $URI .= 'redirect=' . urlencode($handleUrl);
        return $URI;
    }
}
