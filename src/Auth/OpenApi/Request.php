<?php
/**
 * Created by Kieu Trung.
 * User: trung.kieu@epsilo.io
 * Date: 17/11/2020
 * Time: 14:37
 */

namespace Epsilo\Auth\OpenApi;

class Request
{
    public $apiName;

    public $baseUrl;

    public $headerParams = array();

    public $udfParams = array();

    public $fileParams = array();

    public $httpMethod = 'POST';

    public function __construct($apiName, $httpMethod = 'POST')
    {
        $this->apiName = $apiName;
        $this->httpMethod = $httpMethod;
    }

    public function getApiName()
    {
        return $this->apiName;
    }

    public function addApiParam($key, $value)
    {
        if (is_object($value)) {
            $this->udfParams[$key] = json_decode($value);
        } else {
            $this->udfParams[$key] = $value;
        }
    }

    public function addFileParam($key, $content, $mimeType = 'application/octet-stream')
    {
        $file = array(
            'type' => $mimeType,
            'content' => $content,
            'name' => $key
        );
        $this->fileParams[$key] = $file;
    }

    public function addHttpHeaderParam($key, $value)
    {
        $this->headerParams[$key] = $value;
    }

    public function startWith($str, $needle)
    {
        return strpos($str, $needle) === 0;
    }

    /**
     * GET base url
     * @return mixed
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * Set base Url
     * @param $url
     */
    public function setBaseUrl($url)
    {
        $this->baseUrl = $url;
    }
}