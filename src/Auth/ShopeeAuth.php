<?php

namespace Epsilo\Auth;

use Epsilo\Library\Helpers;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use const http\Client\Curl\AUTH_ANY;


class ShopeeAuth extends Auth
{

    static protected $instance = null;

    const API_LOGIN = '/api/v2/login/';
    const API_GET_SIG = '/api/selleraccount/subaccount/get_sig/';
    const API_LIST_SHOP = '/api/selleraccount/subaccount/get_shop_list/';
    const API_SHOP_INFO = '/api/selleraccount/shop_info/';

    const ACCOUNT_TYPE_MAIN = 'main_account';
    const ACCOUNT_TYPE_SUB = 'sub_account';

    static private function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new ShopeeAuth();
        }
        return self::$instance;
    }

    static function isSubAccount($userName)
    {
        if (strpos($userName, ':') !== false) {
            return true;
        }
        return false;
    }

    static function auth($userName, $password, $country, $otp = '', $options = [])
    {

        $isSubAccount = ShopeeAuth::isSubAccount($userName);

        if ($isSubAccount) {
            return ShopeeAuth::loginSubAccount($userName, $password, $country, $otp, $options);
        } else {
            return ShopeeAuth::loginMainAccount($userName, $password, $country, $otp, $options);
        }

    }

    private function getSellerDomain($country)
    {
        $c = [
            self::COUNTRY_VIETNAM => 'https://banhang.shopee.vn',
            self::COUNTRY_PHILIPPINES => 'https://seller.shopee.ph',
            self::COUNTRY_MALAYSIA => 'https://seller.shopee.com.my',
            self::COUNTRY_SINGAPORE => 'https://seller.shopee.sg',
            self::COUNTRY_INDONESIA => 'https://seller.shopee.co.id',
            self::COUNTRY_THAILAND => 'https://seller.shopee.co.th',
        ];

        return $c[$country];
    }

    static function loginSubAccount($userName, $password, $country, $otp = '', $options = [])
    {
        $ins = ShopeeAuth::getInstance();

        $gC = Helpers::getString($options, 'captcha');
        $gCK = Helpers::get($options, 'captcha_key');
        $shopSid = Helpers::getInt($options, 'shop_sid');
        $gCK = ($gCK === null) ? strtolower(Helpers::generateRandomString(32)) : strval($gCK);
        $postData = [
            'captcha' => $gC,
            'captcha_key' => $gCK,
            'remember' => 'false',
            'password_hash' => md5($password),
            'subaccount' => $userName
        ];

        if ($otp) {
            $postData['vcode'] = $otp;
        }

        $randomString = Helpers::generateRandomString(32);

        $domain = $ins->getSellerDomain($country);
        $api = "$domain" . self::API_LOGIN . " ?SPC_CDS=$randomString&SPC_CDS_VER=2";

        $headers = $ins->getCommonHeader();
        $headers['Referer: '] = $domain . '/account/signin';

        $client = new Client();
        try {
            $rs = $client->request("POST", $api, [
                'form_params' => $postData,
                'headers' => $headers
            ]);

            $data = json_decode($rs->getBody()->getContents(), true);
            $cookieString = $ins->getCookie($rs->getHeader('Set-Cookie'));

            if (!$shopSid) {
                $shopSid = intval($data['shopid']);
            }

            $headers['Cookie'] = $cookieString;
            $api = "$domain" . self::API_GET_SIG . "?SPC_CDS_VER=2&SPC_CDS=$randomString&target_shop_id=$shopSid";
            $response = $client->request("GET", $api, ['headers' => $headers]);

            $sigData = json_decode((string)$response->getBody(), true);

            $sigUrl = Helpers::getString($sigData, 'url');
            if ($sigUrl == "") {
                return $ins->error(Auth::ERROR_CAN_NOT_GET_SIG, $sigData);
            }
            $sig = explode('?', $sigUrl)[1] ?? '';
            $sig = explode('=', $sig)[1] ?? '';
            $sig = urldecode($sig);

            $api = "$domain" . self::API_LOGIN . "?SPC_CDS=$randomString&SPC_CDS_VER=2";

            $headers['Referer'] = $domain . '/?' . $sig;

            $response = $client->request("POST", $api, [
                    'form_params' => ['sig' => $sig],
                    'headers' => $headers
                ]
            );

            $cookieString = $ins->getCookie($response->getHeader('Set-Cookie'));
            $headers['Cookie'] = $cookieString;
            list($shopList, $shopRawData) = $ins->getShopList($headers, $country);
            $data['shop_info'] = $shopRawData;

            $shopInfo = $ins->getShopInfo($headers, $country);

            return $ins->success(
                $cookieString,
                self::ACCOUNT_TYPE_SUB,
                $shopSid,
                $shopInfo['shop_name'],
                $shopList,
                $data);

        } catch (ClientException $e) {
            $errorData = json_decode($e->getResponse()->getBody()->getContents(), true);
            $errorMessage = Helpers::getString($errorData, "message");
            if ($e->getCode() == 481 || $e->getCode() == 482) {
                return $ins->error($ins->vCodeMessage($errorMessage), $errorData);
            }
            if ($errorMessage == "error_name_or_password_incorrect") {
                return $ins->error(Auth::ERROR_NAME_OR_PASSWORD_INCORRECT, $errorData);
            }
            return $ins->error($errorMessage, ['client_exception' => $e->getMessage()]);
        } catch (ServerException $e) {
            return $ins->error($e->getMessage());
        }
    }

    private function vCodeMessage($rawMessage)
    {
        $c = [
            'error_need_vcode' => Auth::ERROR_NEED_OTP,
            'error_invalid_vcode' => Auth::ERROR_OTP_INVALID
        ];

        return $c[$rawMessage] ?? AUTH::ERROR_UNKNOWN;
    }

    private function getShopList($headers, $country)
    {

        $domain = $this->getSellerDomain($country);
        $randomString = Helpers::generateRandomString(32);
        $api = "$domain" . self::API_LIST_SHOP . "?SPC_CDS=$randomString&SPC_CDS_VER=2";

        $client = new Client();

        $rs = $client->request("GET", $api, [
            'headers' => $headers
        ]);

        $shops = json_decode($rs->getBody()->getContents(), true)['shops'];
        $shopList = [];

        foreach ($shops as $shop) {
            $shopList[] = [
                'shop_sid' => $shop['shop_id'],
                'shop_name' => $shop['shop_name']
            ];
        }

        return [$shopList, $shops];

    }

    private function getShopInfo($headers, $country)
    {
        $domain = $this->getSellerDomain($country);
        $randomString = Helpers::generateRandomString(32);
        $api = "$domain" . self::API_SHOP_INFO . "?SPC_CDS=$randomString&SPC_CDS_VER=2";

        $client = new Client();

        $rs = $client->request("GET", $api, [
            'headers' => $headers
        ]);

        $body = $rs->getBody()->getContents();
        $shopInfoTmp = json_decode($body, true)['data'];
        return [
            'shop_sid' => $shopInfoTmp['shop_id'],
            'shop_name' => $shopInfoTmp['name']
        ];
    }

    static function loginMainAccount($userName, $password, $country, $otp = '', $options = [])
    {
        $ins = ShopeeAuth::getInstance();

        $gC = Helpers::getString($options, 'captcha');
        $gCK = Helpers::get($options, 'captcha_key');
        $gCK = ($gCK === null) ? strtolower(Helpers::generateRandomString(32)) : strval($gCK);
        $postData = [
            'captcha' => $gC,
            'captcha_key' => $gCK,
            'remember' => 'false',
            'password_hash' => hash('sha256', md5($password))
        ];

        if (is_numeric($userName)) {
            $postData['phone'] = $userName;
        } else {
            if (filter_var($userName, FILTER_VALIDATE_EMAIL)) {
                $postData['email'] = $userName;
            } else {
                $postData['username'] = $userName;
            }
        }

        if ($otp) {
            $postData['vcode'] = $otp;
        }

        $randomString = Helpers::generateRandomString(32);

        $domain = $ins->getSellerDomain($country);
        $api = "$domain" . self::API_LOGIN . "?SPC_CDS=$randomString&SPC_CDS_VER=2";

        $headers = $ins->getCommonHeader();
        $headers['Referer: '] = $domain . '/account/signin';

        $client = new Client();
        try {
            $rs = $client->request("POST", $api, [
                'form_params' => $postData,
                'headers' => $headers
            ]);
            $data = json_decode($rs->getBody()->getContents(), true);
            $shopSid = intval($data['shopid']);
            $cookieString = $ins->getCookie($rs->getHeader('Set-Cookie'));
            $headers['Cookie'] = $cookieString;
            $shopInfo = $ins->getShopInfo($headers, $country);

            return $ins->success(
                $cookieString,
                self::ACCOUNT_TYPE_MAIN,
                $shopSid,
                $shopInfo['shop_name'],
                [$shopInfo],
                $data);

        } catch (ClientException $e) {
            $errorData = json_decode($e->getResponse()->getBody()->getContents(), true);
            $errorMessage = Helpers::getString($errorData, "message");
            if ($e->getCode() == 481 || $e->getCode() == 482) {
                return $ins->error($ins->vCodeMessage($errorMessage), $errorData);
            }
            if ($errorMessage == "error_name_or_password_incorrect") {
                return $ins->error(Auth::ERROR_NAME_OR_PASSWORD_INCORRECT, $errorData);
            }
            return $ins->error($errorMessage, ['client_exception' => $e->getMessage()]);
        } catch (ServerException $e) {
            return $ins->error($e->getMessage());
        }
    }

    static function checkCookieValid($cookieString, $country)
    {
        $ins = self::getInstance();
        $randomString = Helpers::generateRandomString(32);

        $domain = $ins->getSellerDomain($country);
        $api = "$domain" . "/api/selleraccount/user_info/?SPC_CDS=$randomString&SPC_CDS_VER=2";

        $headers = $ins->getCommonHeader();
        $headers['Referer: '] = $domain . '/account/signin';
        $headers['Cookie'] = $cookieString;

        $client = new Client();
        try {
            $rs = $client->request("GET", $api, [
                'headers' => $headers
            ]);
            $data = json_decode($rs->getBody()->getContents(), true);
            return json_encode($data);
        } catch (\Exception $e) {
            return false;
        }
    }
}