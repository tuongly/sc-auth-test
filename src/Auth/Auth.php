<?php


namespace Epsilo\Auth;


class Auth
{
    use AuthResponse;


    const COUNTRY_VIETNAM = 'VN';
    const COUNTRY_PHILIPPINES = 'PH';
    const COUNTRY_MALAYSIA = 'MY';
    const COUNTRY_SINGAPORE = 'SG';
    const COUNTRY_INDONESIA = 'ID';
    const COUNTRY_THAILAND = 'TH';

    const ERROR_NEED_OTP = 'need_otp';
    const ERROR_OTP_INVALID = 'otp_invalid';
    const ERROR_NAME_OR_PASSWORD_INCORRECT = 'name_or_password_incorrect';
    const ERROR_CAN_NOT_GET_SIG = 'can_not_get_sig';
    const ERROR_UNKNOWN = 'error_unknown';

    protected function getCommonHeader()
    {
        $headers = [];
        $headers['User-Agent'] = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0';
        $headers['Accept'] = 'application/json, text/plain, */*';
        $headers['Accept-Language'] = 'en-US,en;q=0.5';
        $headers['Connection'] = 'keep-alive';
        return $headers;
    }

    protected function getCookie($setCookieData)
    {
        if (!is_array($setCookieData)) {
            preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $setCookieData, $matches);
            return implode('; ', $matches[1]);
        } else {
            $r = [];
            foreach ($setCookieData as $item) {
                preg_match_all('/^\s*([^;]*)/mi', $item, $matches);
                $r[] = $matches[1][0] ?? '';
            }
            return implode('; ', $r);
        }
    }

    public static function getNationCode($nation)
    {
        $c = [
            Auth::COUNTRY_VIETNAM => '84',
            Auth::COUNTRY_PHILIPPINES => '63',
            Auth::COUNTRY_MALAYSIA => '60',
            Auth::COUNTRY_SINGAPORE => '65',
            Auth::COUNTRY_INDONESIA => '62',
            Auth::COUNTRY_THAILAND => '66',
        ];
        return $c[$nation];
    }
}