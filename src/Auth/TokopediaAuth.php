<?php


namespace Epsilo\Auth;

use Epsilo\Library\Helpers;
use GuzzleHttp\Client;

class TokopediaAuth extends Auth
{

    static protected $instance = null;

    const API_DOMAIN = 'https://accounts.tokopedia.com';
    const API_LOGIN = '/api/authorize';

    const ACCOUNT_TYPE_MAIN = 'main_account';
    const ACCOUNT_TYPE_SUB = 'sub_account';

    const ERROR_TOKOPEDIA_COOKIE_NOT_VALID = 'error_tokopedia_cookie_not_valid';

    static private function getInstance()
    {
        if (self::$instance == null) {

            self::$instance = new TokopediaAuth();
        }
        return self::$instance;
    }

    static function auth($userName, $password, $country, $otp = '', $options = [])
    {
        $ins = self::getInstance();

        $domain = $options['api_domain_simulation'];
        $postData['email'] = $userName;
        $postData['password'] = $password;
        $postData['country'] = $country;

        $api = $domain . "/tokopedia/auth";
        $headers = $ins->getCommonHeader();
        $client = new Client();
        try {
            $rs = $client->request("POST", $api, [
                'form_params' => $postData,
            ]);

            $body = $rs->getBody()->getContents();
            $data = json_decode($body, true);
            if ($data['success'] === true) {
                $cookieString = $data['cookie_string'];
                $headers['cookie'] = $cookieString;
                $shopInfo = $ins->getShopInfo($headers, $country);
                //hot fix: when the cookie string can not get shopInfo, asume that cookie is not valid
                if ($shopInfo['shop_sid'] == ''){
                    return $ins->error(self::ERROR_TOKOPEDIA_COOKIE_NOT_VALID);
                }
                $accountType = "";
                $shopSid = $shopInfo['shop_sid'];
                $shopName = $shopInfo['shop_name'];
                return $ins->success($cookieString, $accountType, $shopSid, $shopName, [], $shopInfo['raw_data']);
            } else {
                return $ins->error(Auth::ERROR_UNKNOWN);
            }
        } catch (\Exception $e) {
            return $ins->error($e->getMessage());
        }
    }

    static function getShopInfo($headers, $country)
    {
        $ins = self::getInstance();
        $api = $ins->getGQLDomain($country);

        $client = new Client();

        $bodyRaw = '[{"operationName":"isAuthenticatedQuery","variables":{},"query":"query isAuthenticatedQuery {  isAuthenticated  userShopInfo {    info {      shop_id      shop_domain      shop_name      shop_avatar      shop_is_official      shop_score      shop_location      __typename    }    owner {      owner_id      is_gold_merchant      pm_status      __typename    }    __typename  }  user {    name    full_name    profilePicture    __typename  }}"}]';

        try {
            $rs = $client->request("POST", $api, [
                'curl' => [
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_2_0,
                ],
                'headers' => $headers,
                'body' => $bodyRaw,
            ]);
            $body = $rs->getBody()->getContents();
            $data = json_decode($body, true);

            if ($data[0]['data']['isAuthenticated'] != 0) {
                // tuonglv - need to check why has an index 0?
                $_data = $data[0]['data'];
                return [
                    'shop_sid' => $_data['userShopInfo']['info']['shop_id'],
                    'shop_name' => $_data['userShopInfo']['info']['shop_name'],
                    'raw_data' => $_data
                ];
            }
        } catch (\Exception $e) {
            //logger error
        }
        return ['shop_sid' => '', 'shop_name' => '', 'raw_data' => []];

    }

    protected function getSellerDomain($country)
    {
        return "https://gql.tokopedia.com";
    }

    protected function getGQLDomain($country)
    {
        return "https://gql.tokopedia.com/";
    }
    protected function getCommonHeader()
    {
        $headers = [];
        $headers['User-Agent'] = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0';
        $headers['Accept'] = 'application/json, text/plain, */*';
        $headers['Accept-Language'] = 'en-US,en;q=0.5';
        $headers['Connection'] = 'keep-alive';
        return $headers;
    }
}