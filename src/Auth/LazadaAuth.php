<?php

namespace Epsilo\Auth;

use Epsilo\Library\Helpers;
use GuzzleHttp\Client;


class LazadaAuth extends Auth
{

    static protected $instance = null;

    const API_PROFILE = "/profile/your_profile";
    const API_LOGIN = '/seller/login/submit';
    const API_LOGIN_INFO = '/index/getLoginInfo';
    const API_HEADER_INFO = '/site/getHeadInfo/';

    const ACCOUNT_TYPE_MAIN = 'main_account';
    const ACCOUNT_TYPE_SUB = 'sub_account';

    static private function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new LazadaAuth();
        }
        return self::$instance;
    }

    static function auth($userName, $password, $country, $otp = '', $options = [])
    {

        $ins = self::getInstance();

        $domain = $options['api_domain_simulation'];
        $postData['email'] = $userName;
        $postData['password'] = $password;
        $postData['country'] = $country;

        $api = $domain . "/lazada/auth";
        $headers = $ins->getCommonHeader();
        $client = new Client();

        try {
            $rs = $client->request("POST", $api, [
                'form_params' => $postData,
            ]);

            $body = $rs->getBody()->getContents();
            $data = json_decode($body, true);
            if ($data['success'] === true) {
                $cookieString = $data['cookie_string'];

                $headers['Cookie'] = $cookieString;
                $accountType = $data['account_type'];
                $accountInfo = $ins->getAccountInfo($headers, $country);
                $shopName = $ins->getShopName($headers, $country);
                $data['account_info'] = $accountInfo;
                return $ins->success(
                    $cookieString,
                    $accountType,
                    $accountInfo['shop_sid'],
                    $shopName,
                    [],
                    $data);
            } else {
                return $ins->error(Auth::ERROR_UNKNOWN);
            }
        } catch (\Exception $e) {
            return $ins->error($e->getMessage());
        }
    }

    private function getAccountInfo($headers, $country)
    {

        $ins = self::getInstance();
        $api = $ins->getSellerDomain($country) . self::API_LOGIN_INFO;

        $client = new Client();
        try {
            $rs = $client->request("GET", $api, [
                'headers' => $headers
            ]);

            $body = $rs->getBody()->getContents();
            $data = json_decode($body, true);

            if ($data['success'] == true) {
                return [
                    'user_id' => Helpers::getString($data, 'result', 'userId'),
                    'shop_sid' => $data['result']['shopId'],
                    'seller_id' => Helpers::getString($data, 'result', 'sellerId'),
                ];
            }

        } catch (\Exception $e) {

        }
        return ['user_id' => '', 'shop_sid' => '', 'seller_id' => ''];
    }

    private function getShopName($headers, $country)
    {
        $ins = self::getInstance();
        $api = $ins->getSellerDomain($country) . self::API_HEADER_INFO;

        $client = new Client();
        $shopName = "";
        try {
            $rs = $client->request("GET", $api, [
                'headers' => $headers
            ]);

            $data = json_decode($rs->getBody()->getContents(), true);
            $shopName = $data['result']['config']['baseInfo']['shopName'];
        } catch (\Exception $e) {

        }
        return $shopName;
    }

    private function getAccountType($headers, $country)
    {

        $ins = self::getInstance();
        $api = $ins->getSellerDomain($country) . self::API_PROFILE;

        $client = new Client(['allow_redirects' => ['track_redirects' => true]]);
        try {
            $rs = $client->request("GET", $api, [
                'headers' => $headers
            ]);

            $headersRedirect = $rs->getHeader(\GuzzleHttp\RedirectMiddleware::HISTORY_HEADER);

            if ($headersRedirect == []) {
                return self::ACCOUNT_TYPE_MAIN;
            }

        } catch (\Exception $e) {

        }
        return self::ACCOUNT_TYPE_SUB;
    }

    private function getSellerDomain($country)
    {
        $c = [
            self::COUNTRY_VIETNAM => 'https://sellercenter.lazada.vn',
            self::COUNTRY_PHILIPPINES => 'https://sellercenter.lazada.com.ph',
            self::COUNTRY_MALAYSIA => 'https://sellercenter.lazada.com.my',
            self::COUNTRY_SINGAPORE => 'https://sellercenter.lazada.sg',
            self::COUNTRY_INDONESIA => 'https://sellercenter.lazada.co.id',
            self::COUNTRY_THAILAND => 'https://sellercenter.lazada.co.th',
        ];

        return $c[$country];
    }

    private function getASCDomain($country)
    {
        $c = [
            self::COUNTRY_VIETNAM => 'https://acs-m.lazada.vn',
            self::COUNTRY_PHILIPPINES => 'https://acs-m.lazada.com.ph',
            self::COUNTRY_MALAYSIA => 'https://acs-m.lazada.com.my',
            self::COUNTRY_SINGAPORE => 'https://acs-m.lazada.sg',
            self::COUNTRY_INDONESIA => 'https://acs-m.lazada.co.id',
            self::COUNTRY_THAILAND => 'https://acs-m.lazada.co.th',
        ];

        return $c[$country];
    }

    static function checkCookieValid($cookieString, $country)
    {
        $ins = self::getInstance();


        $domain = $ins->getSellerDomain($country);
        $api = "$domain" . "/index/getLoginInfo";

        $headers = $ins->getCommonHeader();
        $headers['Cookie'] = $cookieString;

        $client = new Client();
        try {
            $rs = $client->request("GET", $api, [
                'headers' => $headers
            ]);
            $data = json_decode($rs->getBody()->getContents(), true);
            return json_encode($data);
        } catch (\Exception $e) {
            return false;
        }
    }
}