<?php

namespace Epsilo\Auth;


use Epsilo\Library\Helpers;
use GuzzleHttp\Client;

class SendoAuth
{

    use AuthResponse;

    //const API_AUTHEN = "https://oauth.sendo.vn/login/sendoid";

    static private $instance = null;

    static private function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new SendoAuth();
        }
        return self::$instance;
    }

    static function auth($userName, $password, $options = [])
    {
        $ins = SendoAuth::getInstance();

        $client = new Client();

        $gR = Helpers::getString($options, 'g-recaptcha-response');
        $gT = Helpers::get($options, 'g-recaptcha-type');
        $gS = Helpers::get($options, 'source');

        $gS = ($gS === null) ? 3 : $gS;
        $gT = ($gT === null) ? "invisible" : $gT;
        $postData = [
            "username" => $userName,
            "password" => $password,
            "source" => $gS,
            "g-recaptcha-response" => $gR,
            "g-recaptcha-type" => $gT,
        ];

        try {
            $rs = $client->request("POST", "https://oauth.sendo.vn/login/sendoid", [
                'body' => json_encode($postData),
            ]);
            $body = $rs->getBody()->getContents();
            $data = json_decode($body, true);

        } catch (\Exception $e) {
            return $ins->error($e->getMessage());
        }

        if (!$body) {
            return $ins->error("can not parse content", $body);
        }

        $token = Helpers::get($data, 'result', 'metaData', 'token');
        if ($token) {
            return $ins->success($token, $data);
        } else {
            return $ins->error("something wrong", $data);
        }
    }
}