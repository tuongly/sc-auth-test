<?php
/**
 * Created by Kieu Trung.
 * User: trung.kieu@epsilo.io
 * Date: 17/11/2020
 * Time: 14:31
 */

namespace Epsilo\Library;


class CurlBuilder
{
    /**
     * @var resource
     */
    protected $ch;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $method = 'GET';

    /**
     * @var bool
     */
    protected $typeJson = false;

    /**
     * @var mixed
     */
    protected $data;

    /**
     * @var bool
     */
    protected $isRawPostData = false;

    /**
     * @var array
     */
    protected $header = [];

    /**
     * @var int
     */
    protected $timeout = 30;

    /**
     * @var string
     */
    private $_requestBody = '';

    /**
     * @var string
     */
    private $_requestUrl = '';

    /**
     * @var string
     */
    private $_responseRaw = '';

    /**
     * @var int
     */
    private $_requestAt = null;

    /**
     * @var int
     */
    private $_responseAt = null;

    /**
     * @var string
     */
    private $_errorMsg = null;

    /**
     * @param int $timeout
     * @return \App\Library\Application\CurlBuilder
     */
    public function setTimeout($timeout)
    {
        $this->timeout = intval($timeout);
        return $this;
    }

    public function __construct()
    {
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
    }

    public function header($header)
    {
        $this->header[] = $header;
        return $this;
    }

    public function isJson($charset = '')
    {
        $this->typeJson = true;
        $header = 'Content-Type: application/json';
        if ($charset) {
            $header .= '; charset=' . $charset;
        }
        $this->header($header);
        return $this;
    }

    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    public function setMethod($method)
    {
        $this->method = strtoupper($method);
        return $this;
    }

    public function setParam($data)
    {
        $this->data = $data;
        return $this;
    }

    public function follow()
    {
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
        return $this;
    }

    public function setIsRawPostData()
    {
        $this->isRawPostData = true;
        return $this;
    }

    /**
     * @param bool $isDecodeJson
     * @return bool|mixed|string
     */
    public function execute($isDecodeJson = true)
    {
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->header);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, $this->method);
        $data = null;
        if ($this->data) {
            if (!$this->isRawPostData) {
                if ($this->typeJson) {
                    $data = json_encode($this->data);
                } else {
                    $data = http_build_query($this->data);
                }
            } else {
                $data = $this->data;
            }
        }
        switch ($this->method) {
            case 'GET':
                if ($data) {
                    curl_setopt($this->ch, CURLOPT_URL, $this->url.'?'.$data);
                    $this->_requestUrl = $this->url.'?'.$data;
                } else {
                    curl_setopt($this->ch, CURLOPT_URL, $this->url);
                    $this->_requestUrl = $this->url;
                }
                break;

            default:
                curl_setopt($this->ch, CURLOPT_URL, $this->url);
                $this->_requestUrl = $this->url;
                curl_setopt($this->ch, CURLOPT_POST, 1);
                if ($data) {
                    curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data);
                    $this->_requestBody = $data;
                }
                break;
        }

        curl_setopt($this->ch, CURLOPT_TIMEOUT, $this->timeout);
        #curl_setopt($this->ch, CURLOPT_VERBOSE, 1);
        $this->_requestAt = strtotime("now");

        $response = curl_exec($this->ch);
        #echo PHP_EOL, $response, PHP_EOL;
        $this->_responseAt = strtotime("now");
        $this->_errorMsg = curl_error($this->ch);
        if ($isDecodeJson) {
            return json_decode($response, true);
        }
        return $response;
    }

    public function __destruct()
    {
        curl_close($this->ch);
    }

    /**
     * Get file
     * @return bool|string
     */
    public function getFile()
    {
        curl_setopt($this->ch, CURLOPT_URL, $this->url);
        curl_setopt($this->ch, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
        $data = curl_exec($this->ch);
        $this->_requestAt = strtotime("now");
        $response = curl_exec($this->ch);
        $this->_responseAt = strtotime("now");
        if (!curl_error($this->ch)) {
            $this->_responseRaw = $response;
        }
        return $data;
    }

    public function getFileWithParam()
    {
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->header);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, $this->method);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);

        $data = null;
        if ($this->data) {
            if (!$this->isRawPostData) {
                if ($this->typeJson) {
                    $data = json_encode($this->data);
                } else {
                    $data = http_build_query($this->data);
                }
            } else {
                $data = $this->data;
            }
        }
        switch ($this->method) {
            case 'GET':
                if ($data) {
                    curl_setopt($this->ch, CURLOPT_URL, $this->url.'?'.$data);
                } else {
                    curl_setopt($this->ch, CURLOPT_URL, $this->url);
                }
                break;

            default:
                curl_setopt($this->ch, CURLOPT_URL, $this->url);
                curl_setopt($this->ch, CURLOPT_POST, 1);
                if ($data) {
                    curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data);
                }
                break;
        }

        $response = curl_exec($this->ch);
        return $response;
    }

    public function getContentTypeFile()
    {
        $contentType = curl_getinfo($this->ch, CURLINFO_CONTENT_TYPE);
        return $contentType;
    }


    public function authBasic($username, $password)
    {
        curl_setopt($this->ch, CURLOPT_USERPWD, $username . ":" . $password);
        return $this;
    }
}