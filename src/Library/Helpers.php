<?php

namespace Epsilo\Library;


use Epsilo\Auth\Auth;

class Helpers
{
    public static function get($data, ...$keys)
    {
        foreach ($keys as $key) {
            @$data = $data[$key];
        }
        $data = ($data === null) ? null : $data;
        return $data;
    }

    public static function getInt($data, ...$keys)
    {
        foreach ($keys as $key) {
            @$data = $data[$key];
        }
        $data = ($data === null) ? 0 : intval($data);
        return $data;
    }

    public static function getString($data, ...$keys)
    {
        foreach ($keys as $key) {
            @$data = $data[$key];
        }
        $data = ($data === null) ? "" : strval($data);
        return $data;
    }

    public static function getArray($data, ...$keys)
    {
        foreach ($keys as $key) {
            @$data = $data[$key];
        }
        $data = ($data === null) ? [] : $data;
        return $data;
    }

    public static function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function isPhoneAccount($account){
        // maybe not correct
        if (is_numeric($account)) {
            return true;
        }
        return false;
    }

}