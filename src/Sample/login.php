<?php
include('vendor/autoload.php');

$userName = "";
$password = "";
$otp = "";


$rs1 = \Epsilo\Auth\ShopeeAuth::auth($userName, $password, Epsilo\Auth\Auth::COUNTRY_VIETNAM);
$rs2 = \Epsilo\Auth\ShopeeAuth::auth($userName, $password, Epsilo\Auth\Auth::COUNTRY_VIETNAM, $otp);
$rs3 = \Epsilo\Auth\ShopeeAuth::auth($userName, $password, Epsilo\Auth\Auth::COUNTRY_VIETNAM, $otp, ['shop_sid' => 123]);

//api_domain_simulation get from .env
$rs4 = \Epsilo\Auth\TokopediaAuth::auth($userName, $password, "VN", "", [
    "api_domain_simulation" => "http://35.247.172.164:5000"
]);


//api_domain_simulation get from .env
$rs5 = \Epsilo\Auth\LazadaAuth::auth($userName, $password, "VN", "", [
    "api_domain_simulation" => "http://35.247.172.164:5000"
]);